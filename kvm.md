# VLAN Bridges in Manjaro

A quick one liner to add a VLAN to the bridges

```
bridge link show | grep -i virbr2 | awk -F':' '{print $2}' | xargs -I{} sudo bridge vlan add vid $ID dev {}; sudo bridge vlan add vid $ID dev virbr2 self; bridge vlan show
```
