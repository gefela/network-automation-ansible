# Network Automation with Ansible

This repository contains the Ansible playbooks, roles and variables for my Ansible for Networks Series on my [blog](https://yetiops.net)

Each directory that is finished will also have a **configs** directory, with the final generated configuration running on each device.

# Completed

* [CentOS Route Server build with FRR, TACACS+ and Syslog](/centos)
* [Cisco IOS](/cisco)
* [Juniper JunOS](/junos)
* [Arista EOS](/arista)

# Work-in-Progress

* MikroTik
* VyOS
* Extreme EXOS
* Cumulus
